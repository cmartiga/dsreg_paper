import numpy as np
import pandas as pd
from statsmodels.stats.multitest import multipletests
import statsmodels.api as sm
import statsmodels.formula.api as smf
from utils.settings import ORA


def padjust(p_values, method='fdr_bh'):
    if p_values.shape[0] == 0:
        return([])
    return(multipletests(p_values, method=method)[1])


def rbp_glm(rbp, group):
    if group.sum() == 0 or rbp.sum() == 0:
        pvalue = np.nan
        estimate = np.nan
    else:
        data = pd.DataFrame({'group': group, 'rbp': rbp})
        model = smf.glm(formula='group ~ rbp', data=data,
                        family=sm.families.Binomial())
        results = model.fit()
        pvalue = results.pvalues[-1]
        estimate = results.params[-1]
    return(estimate, pvalue)


def make_dAS_groups(data, fdr_threshold=0.05):
    sig = data['fdr'] < fdr_threshold
    data['group'] = 'No-change'
    up = np.logical_and(sig, data['estimate'] > 0)
    down = np.logical_and(sig, data['estimate'] < 0)
    data.loc[up, 'group'] = 'Included'
    data.loc[down, 'group'] = 'Skipped'


def run_ORA(results, binding_sites, label, fdr_threshold=0.05):
    if isinstance(binding_sites, pd.DataFrame):
        bs = binding_sites.loc[results['id'], :]
        rbps = {rbp: np.array(binding_sites[rbp]).flatten()
                for rbp in binding_sites.columns}
    else:
        sel_ids = [int(x) for x in results['id']]
        bs = binding_sites[sel_ids, :]
        rbps = {i: np.array(bs[:, i]).flatten()
                for i in range(binding_sites.shape[1])} 
        
    make_dAS_groups(results, fdr_threshold=0.05)
    group = (results['group'] == label).astype(int)

    for rbp_name, rbp in rbps.items():
        estimate, pvalue = rbp_glm(rbp, group)
        record = {'rbp': rbp_name, 'estimate': estimate, 'pvalue': pvalue,
                  'group': label, 'fdr_threshold': fdr_threshold, 'method': ORA}
        yield(record)


def to_df(records):
    df = pd.DataFrame(list(records))
    sel_rows = np.isnan(df['pvalue']) == False
    df['fdr'] = np.nan
    df.loc[sel_rows, 'fdr'] = padjust(df.loc[sel_rows, 'pvalue'])
    return(df)


def calc_enrichment_scores(sorted_idxs, scores):
    if isinstance(scores, pd.DataFrame):
        s = np.cumsum(scores.loc[sorted_idxs, :], axis=0)
    else:
        s = np.cumsum(scores[sorted_idxs, :], axis=0)
    return(s.min(0), s.max(0))


def calc_null_enrichment_scores(scores, n=10000, log=None):
    if hasattr(scores, 'index'):
        rownames = np.array(scores.index)
    else:
        rownames = np.arange(scores.shape[0])
    es_nulls = [[], []]
    if log is not None:
        log.write('Start permutations for ES null distribution')
    for i in range(n):
        if log is not None and i % 500 == 0:
            log.write('Permutation done: {}'.format(i))
        np.random.shuffle(rownames)
        es_min, es_max = calc_enrichment_scores(rownames, scores)
        es_nulls[0].append(es_min)
        es_nulls[1].append(es_max)
    return(np.array(es_nulls[0]), np.array(es_nulls[1]))


def run_gsea(results, binding_sites, npermutations=10000, log=None):
    sorted_exon_ids = results.sort_values('estimate')['id']

    if isinstance(binding_sites, pd.DataFrame):
        scores = binding_sites.loc[sorted_exon_ids, :].as_matrix()
    else:
        scores = binding_sites[sorted_exon_ids, :]
        
    scores = scores - scores.mean(0)
    rownames = np.arange(scores.shape[0])

    if log is not None:
        log.write('Calculating Enrichment scores for data')

    es_min, es_max = calc_enrichment_scores(rownames, scores)
    null_es_min, null_es_max = calc_null_enrichment_scores(scores,
                                                           n=npermutations,
                                                           log=log)
    p = 2 * np.min(np.vstack([np.mean(es_min < null_es_min, 0),
                              np.mean(es_max > null_es_max, 0)]), axis=0)

    for i in range(p.shape[0]):
        if np.abs(es_min[i]) > np.abs(es_max[i]):
            estimate = es_min[i]
        else:
            estimate = es_max[i]

        record = {'rbp': binding_sites.columns[i],
                  'estimate': estimate, 'pvalue': min(p[i], 1)}
        yield(record)
