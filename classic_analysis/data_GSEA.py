from os.path import join

import numpy as np
import pandas as pd
from utils.settings import CM_DIR, RESULTS_DIR, CM_MATURATION
from utils.utils import LogTrack
from classic_analysis.enrichment import to_df, run_gsea


if __name__ == '__main__':
    # Parameters
    event_type = 'exon_cassette'
    
    # Initialize log
    log = LogTrack()
    log.write('Start GSEA analysis on the CM dataset...')

    # Load data
    log.write('Analysing dataset {}'.format(event_type))
    log.write('Loading data...')
    fname = '{}.binding_sites.csv'.format(event_type)
    fpath = join(CM_DIR, fname)
    binding_sites = pd.read_csv(fpath, index_col=0)
    fpath = join(CM_DIR, '{}.dAS.csv'.format(event_type))
    results = pd.read_csv(fpath)
    
    # Run GSEA
    log.write('\tPerform GSEA')
    res = run_gsea(results, binding_sites, npermutations=20000, log=log)
    df = to_df(res)
    df['event_type'] = event_type
    nans = np.isnan(df['pvalue']).sum()
    if nans > 0:
        msg = '\t{} tests out of {} could not be calculated'
        log.write(msg.format(nans, df.shape[0]))

    # Write output
    fpath = join(RESULTS_DIR,
                 '{}.{}.gsea.csv'.format(CM_MATURATION, event_type))
    df.to_csv(fpath)
    log.finish()
