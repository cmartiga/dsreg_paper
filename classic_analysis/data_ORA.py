from os.path import join

import numpy as np
import pandas as pd
from utils.settings import (CM_DIR, CM_MATURATION, RESULTS_DIR, FDR_THRESHOLDS,
                            ORA)
from utils.utils import LogTrack
from classic_analysis.enrichment import to_df, run_ORA


if __name__ == '__main__':
    # Parameters
    event_type = 'exon_cassette'
    
    # Initialize log
    log = LogTrack()
    log.write('Start classical enrichment analyses...')
    
    # Load data
    log.write('Analysing dataset {}'.format(event_type))
    log.write('Loading data...')
    fname = '{}.binding_sites.csv'.format(event_type)
    fpath = join(CM_DIR, CM_MATURATION, fname)
    binding_sites = pd.read_csv(fpath, index_col=0)
    fpath = join(CM_DIR, CM_MATURATION, '{}.dAS.csv'.format(event_type))
    results = pd.read_csv(fpath)

    # Run ORA on different subsets
    dfs = []
    for group in ['Included', 'Skipped']:
        for threshold in FDR_THRESHOLDS:
            msg = '\tPerform enrichment analysis for {} with FDR<{}'
            log.write(msg.format(group, threshold))
            res = run_ORA(results, binding_sites, label=group,
                          fdr_threshold=threshold)
            df = to_df(res)
            df['event_type'] = event_type
            dfs.append(df)
            nans = np.isnan(df['pvalue']).sum()
            if nans > 0:
                msg = '\t{} tests out of {} could not be calculated'
                log.write(msg.format(nans, df.shape[0]))

    # Merge and write output files
    df = pd.concat(dfs)
    fname = '{}.{}.{}.csv'.format(CM_MATURATION, event_type, ORA) 
    fpath = join(RESULTS_DIR, fname)
    df.to_csv(fpath)
    log.finish()
