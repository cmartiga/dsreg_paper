from os.path import join

import numpy as np
import pandas as pd
from utils.settings import (SIMULATIONS_DIR, CONFIG_FPATH, RESULTS_DIR,
                            FDR_THRESHOLDS)
from utils.utils import LogTrack
from classic_analysis.enrichment import run_ORA, to_df , padjust


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Start classical enrichment analyses...')
    
    # Load simulations data
    config = pd.read_csv(CONFIG_FPATH)
    
    # Run analysis
    dfs = []
    for dataset_id in config['dataset_id']:
        
        # Load input data
        log.write('Analysing dataset {}'.format(dataset_id))
        log.write('Loading data...')
        fname = '{}.binding_sites.csv'.format(dataset_id)
        fpath = join(SIMULATIONS_DIR, fname)
        binding_sites = pd.read_csv(fpath, index_col=0).as_matrix()
        fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
        results = pd.read_csv(fpath)

        # Run ORA for different groups
        for threshold in FDR_THRESHOLDS:
            n_sig = (results['fdr'] < threshold).sum()
            records = []
            for group in ['Included', 'Skipped']:
                msg = '\tPerform enrichment analysis for {} with FDR<{}'
                log.write(msg.format(group, threshold))
                res = run_ORA(results, binding_sites, label=group,
                              fdr_threshold=threshold)
                for record in res:
                    records.append(record)
            df = to_df(records)
            df['dataset_id'] = dataset_id
            df['fdr'] = np.nan
            
            sel_rows = np.isnan(df['pvalue']) == False 
            df.loc[sel_rows, 'fdr'] = padjust(df.loc[sel_rows, 'pvalue'])
            dfs.append(df)
            nans = (sel_rows == False).sum()
            if nans > 0:
                msg = '\t{} tests out of {} could not be calculated. '
                msg += 'Total number of significant events: {}'.format(n_sig)
                log.write(msg.format(nans, df.shape[0]))

    # Merge and write output
    df = pd.concat(dfs)
    fpath = join(RESULTS_DIR, 'simulations_ORA.results.csv')
    df.to_csv(fpath)
    log.finish()
