from os.path import join

from classic_analysis.simulations_ORA import to_df
import numpy as np
import pandas as pd
from utils.settings import (SIMULATIONS_DIR, CONFIG_FPATH, RESULTS_DIR)
from utils.utils import LogTrack
from classic_analysis.enrichment import run_gsea


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Start GSEA analyses...')
    
    # Load simulations data
    config = pd.read_csv(CONFIG_FPATH)
    
    # Run analysis
    dfs = []
    for dataset_id in config['dataset_id']:
        # Load input data
        log.write('Analysing dataset {}'.format(dataset_id))
        log.write('Loading data...')
        fname = '{}.binding_sites.csv'.format(dataset_id)
        fpath = join(SIMULATIONS_DIR, fname)
        binding_sites = pd.read_csv(fpath, index_col=0).as_matrix()
        fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
        results = pd.read_csv(fpath)

        # Run GSEA analysis
        res = run_gsea(results, binding_sites, npermutations=10000)
        df = to_df(res)
        df['dataset_id'] = dataset_id
        dfs.append(df)
        nans = np.isnan(df['pvalue']).sum()
        if nans > 0:
            msg = '\t{} tests out of {} could not be calculated'
            log.write(msg.format(nans, df.shape[0]))
            
    # Merge and write output
    df = pd.concat(dfs)
    fpath = join(RESULTS_DIR, 'simulations_GSEA.results.csv'.format(dataset_id))
    df.to_csv(fpath)
    log.finish()
