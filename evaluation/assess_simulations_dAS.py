from csv import DictWriter
from os.path import join, exists
from sklearn.metrics.classification import (f1_score, precision_score, 
                                            recall_score)
from sklearn.metrics.ranking import roc_auc_score, roc_curve
import numpy as np
import pandas as pd
from utils.settings import (CONFIG_FPATH, SIMULATIONS_DIR, RESULTS_DIR, 
                            FDR_THRESHOLDS, DSREG_METHODS)
from utils.utils import LogTrack
from scipy.stats.stats import pearsonr


def add_threshold_classes(results, thresholds=[0.2, 0.05]):
    for threshold in thresholds:
        fieldname = 'FDR<{}'.format(threshold)
        results[fieldname] = 'No-change'
        sig = results['fdr'] < threshold
        sel_rows = np.logical_and(sig, results['estimate'] > 0)
        results.loc[sel_rows, fieldname] = 'Included'
        sel_rows = np.logical_and(sig, results['estimate'] < 0)
        results.loc[sel_rows, fieldname] = 'Skipped'


def load_dataset_data(dataset_id):
    fpath = join(SIMULATIONS_DIR, '{}.beta.csv'.format(dataset_id))
    reg_effects = np.array(pd.read_csv(fpath, index_col=0).iloc[:, 0])

    fpath = join(SIMULATIONS_DIR, '{}.dAS.csv'.format(dataset_id))
    results = pd.read_csv(fpath)
    results['beta'] = results['estimate']
    return(reg_effects, results)


def parse_model_results(dataset_id, model):
    fpath = join(SIMULATIONS_DIR, '{}.{}.csv'.format(dataset_id, model))
    if not exists(fpath):
        return(None, None)
    
    posterior = pd.read_csv(fpath, usecols=lambda x: x.startswith('beta.'))
    colnames = posterior.columns
    event_ids = [int(name.split('.')[-1]) - 1 for name in colnames]
    p = np.vstack([(posterior > 0).mean(0), (posterior < 0).mean(0)])
    p = np.max(p, axis=0)
    p = 1 - p
    
    data = pd.DataFrame({'p': p, model: 'No-change', 'id': event_ids,
                         'beta': posterior.mean(0)})
    ci = np.percentile(posterior, q=[2.5, 97.5], axis=0)
    return(data, ci)


def calc_specificty(ypred, targets):
    equal = ypred == targets
    positive = ypred == 0
    sp =np.logical_and(positive, equal).sum() / (1 - targets).sum()
    return(sp)


def calc_metrics(results, betas, method, fdr_threshold=0.05, ci=None):
    if method == 'GLM':
        label = 'fdr'
    elif 'dS' in method:
        label = 'p'
    else:
        raise ValueError('Method {} is not valid'.format(method))
    scores = 1 - results[label]
    ypred = (results[label] < fdr_threshold).astype(int)
    sel_idxs = np.array([int(x) for x in results['id']])
    ordered_betas = betas[sel_idxs]
    targets = (ordered_betas != 0).astype(int)
    
    if ci is None:
        calibration = None
    else:
        calibration = np.mean(np.logical_and(ci[0] < ordered_betas,
                                             ci[1] > ordered_betas))

    record = {'specificity': calc_specificty(ypred, targets),
              'f1': f1_score(targets, ypred),
              'roc_auc': roc_auc_score(targets, scores),
              'precision': precision_score(targets, ypred),
              'recall': recall_score(targets, ypred),
              'method': method,
              'threshold': fdr_threshold,
              'calibration': calibration, 
              'rho': pearsonr(ordered_betas, results['beta'])[0]}

    fpr, tpr, _ = roc_curve(targets, scores)
    roc = pd.DataFrame({'tpr': tpr, 'fpr': fpr,
                        'method': method, 'threshold': fdr_threshold})
    return(record, roc)


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Start processing differential AS results...')
    
    # Load simulations configuration
    config = pd.read_csv(CONFIG_FPATH).set_index('dataset_id')
    config = config.to_dict(orient='index')

    # Start output file writer
    fpath = join(RESULTS_DIR, 'simulations_dAS.results.csv')
    rocs = []
    
    with open(fpath, 'w') as fhand:
        fieldnames = ['dataset_id', 'recall', 'precision', 'specificity',
                      'f1', 'roc_auc', 'method', 'loglambda', 'threshold',
                      'calibration', 'rho']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()

        #  Iterate over simulated datasets
        for dataset_id, dataset_data in config.items():
            log.write('Load dataset {}'.format(dataset_id))
            beta, results = load_dataset_data(dataset_id)
            n_events = config[dataset_id]['n_events']
            
            # Load and analyze GLM results
            for t in FDR_THRESHOLDS:
                metrics, roc = calc_metrics(results, beta, method='GLM',
                                            fdr_threshold=t)
                 
                # dAS identification metrics
                metrics['dataset_id'] = dataset_id
                metrics['loglambda'] = dataset_data['loglambda']
                writer.writerow(metrics)
 
                # ROC curve
                roc['loglambda'] = dataset_data['loglambda']
                roc['dataset_id'] = dataset_id
                rocs.append(roc)

            # Load and analyze dSreg models results
            for model in DSREG_METHODS:
                try:
                    mod_dataset, ci = parse_model_results(dataset_id,
                                                          model=model)
                except:
                    log.write('Results for {} not found'.format(model))
                    continue
                if mod_dataset is None:
                    log.write('Results for {} not found'.format(model))
                    continue
                metrics, roc = calc_metrics(mod_dataset, beta,
                                            method=model, ci=ci)

                # dAS identification metrics
                metrics['dataset_id'] = dataset_id
                metrics['loglambda'] = config[dataset_id]['loglambda']
                writer.writerow(metrics)

                # ROC curve
                roc['loglambda'] = config[dataset_id]['loglambda']
                roc['dataset_id'] = dataset_id
                rocs.append(roc)

    # Merge and write output
    roc = pd.concat(rocs)
    fpath = join(RESULTS_DIR, 'simulations_dAS.roc_curves.csv')
    roc.to_csv(fpath)
    log.finish()
