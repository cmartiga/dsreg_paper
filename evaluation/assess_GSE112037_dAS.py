#ª/usr/bin/env python
from os.path import join

import numpy as np
import pandas as pd
from utils.utils import LogTrack
from utils.settings import (GEO_DIR, DARTS_DIR, GLM_DIR, DSREG_DIR, RESULTS_DIR,
                            BRIE_DIR, MISO_DIR, BRIE_IDS_FPATH)
from scipy.stats.stats import pearsonr
from sklearn.metrics.ranking import roc_auc_score
from sklearn.metrics.classification import (recall_score, precision_score,
                                            f1_score)
from sklearn.metrics.regression import mean_squared_error
from evaluation.match_event_ids import parse_MISO_id, format_exon_id


def load_dataset(subsampling, method):
    if 'DARTS' in method: 
        darts_dir = join(DARTS_DIR, 'sample.{}'.format(subsampling))
        if method == 'DARTS-flat':
            fpath = join(darts_dir, 'SE.darts_bht.flat.txt')
        else:
            fpath = join(darts_dir, 'SE.darts_bht.info.txt')
        df = pd.read_csv(fpath, sep='\t', index_col=0)
        df.index = [x.lstrip('chr') for x in df.index]
        dpsi = df['delta.mle']
        p = df['post_pr']
    
    elif 'PW' in method:
        if method == 'dS-PW':
            fname = 'ds_pw.{}.2000.events.csv'.format(subsampling)
        elif method == 'dSreg-PW':
            fname = 'dsreg_pw.{}.2000.events.csv'.format(subsampling)
        else:
            raise ValueError('Method not supported {}'.format(method))
        fpath = join(DSREG_DIR, fname)
        df = pd.read_csv(fpath, index_col=0)
        df.index = [x.lstrip('chr') for x in df.index]
        dpsi = df['E[dPSI]']
        p = df['P(|dPSI|>0.05)']
        
    elif method == 'GLM':
        fpath = join(GLM_DIR, 'sample.{}.dAS_results.csv'.format(subsampling))
        df = pd.read_csv(fpath, index_col=0).set_index('id')
        df.index = [x.lstrip('chr') for x in df.index]
        dpsi = df['dPSI']
        p = 1 - df['fdr']
    elif method == 'BRIE':
        fpath = join(BRIE_DIR, 'differential_AS.{}.tsv'.format(subsampling))
        df = pd.read_csv(fpath, sep='\t')
        
        brie_ids = pd.read_csv(BRIE_IDS_FPATH).set_index('brie_id')
        df.index = [x.lstrip('chr')
                    for x in brie_ids.loc[df['gene_id'], 'event_id']]
        # Typo in BRIE program
        dpsi = df['psi2'] - df['pis1']
        
        # Transformation to a p-value like metric
        p = df['Bayes_factor'] / (1 + df['Bayes_factor'])
    elif method == 'MISO':
        comparison = 'group1.{}_vs_group2.{}'.format(subsampling, subsampling)
        fpath = join(MISO_DIR, 'group1_vs_group2.{}'.format(subsampling),
                     comparison, 'bayes-factors',
                     '{}.miso_bf'.format(comparison))
        df = pd.read_csv(fpath, sep='\t')
        if df.shape[0] == 0:
            raise FileNotFoundError
        df.index = [format_exon_id(parse_MISO_id(miso_id)).lstrip('chr')
                    for miso_id in df['event_name']]
        dpsi = -df['diff']
        
        # Transformation to a p-value like metric
        p = df['bayes_factor'] / (1 + df['bayes_factor'])
    
    else:
        raise ValueError('Method not supported: {}'.format(method))

    return(dpsi, p)


def calc_metrics(dpsi, p, real_dspi, changed, metrics={}):
    # Set common IDs
    sel_ids = np.intersect1d(real_dspi.index, dpsi.index)
    real = real_dspi.loc[sel_ids]
    dpsi = dpsi.loc[sel_ids]
    
    sel_ids = np.intersect1d(changed.index, p.index)
    p = p.loc[sel_ids]
    changed = changed.loc[sel_ids]

    # Make change categories    
    pred = p > 0.95

    # Calculate metrics
    metrics.update({'rho': pearsonr(real, dpsi)[0],
                    'MSE': mean_squared_error(real, dpsi),
                    'AUROC': roc_auc_score(changed, p),
                    'Recall': recall_score(changed, pred),
                    'Precision': precision_score(changed, pred),
                    'F1 score': f1_score(changed, pred)})
    return(metrics)

if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Evaluation of analysis performed on GSE112037 dataset')
    
    # Load validation RASL-seq data
    fpath = join(GEO_DIR, 'dAS_results_RASL.csv')
    rasl = pd.read_csv(fpath).set_index('id')
    rasl.dropna(inplace=True)
    rasl.index = [x.lstrip('chr') for x in rasl.index]
    rasl_changed = np.logical_and(np.abs(rasl['dPSI']) > 0.05,
                                  rasl['fdr'] < 0.05).astype(int)
    log.write('Loaded RASL-seq GOLD-standard measures from {}'.format(fpath))
    
    # Load validation full coverage RNA-seq data
    fpath = join(DARTS_DIR, 'sample.1', 'SE.darts_bht.flat.txt')
    full_data = pd.read_csv(fpath, index_col=0, sep='\t')
    full_data.index = [x.lstrip('chr') for x in full_data.index]
    full_changed = (full_data['post_pr'] > 0.95).astype(int)
    full_changed = full_changed.loc[np.logical_or(full_data['post_pr'] > 0.99,
                                                  full_data['post_pr'] < 0.01)]
    log.write('Loaded full coverage RNA-seq measures from {}'.format(fpath))

    # Load and assess data
    log.write('Evaluating PSI estimations in serieal subsampling:')
    methods = ['DARTS-flat', 'DARTS-info', 'GLM', 'dS-PW',
               'dSreg-PW', 'BRIE', 'MISO']
    subsampling = [2 ** (-x) for x in range(1, 10)]
    metrics = []
    for s in subsampling:
        log.write('\tFraction of total reads: {}'.format(s))
        for method in methods:
            log.write('\t\tEvaluating performance of {}'.format(method))
            try:
                dpsi, p = load_dataset(s, method)
            except FileNotFoundError:
                log.write('\t\t Dataset for {} not found'.format(method))
                continue    
            
            # Assess with RASL-seq 
            data = {'Method': method, 'Dilution': int(1 / s),
                    'Validation': 'RASL-seq'}
            metrics.append(calc_metrics(dpsi, p, rasl['dPSI'], rasl_changed,
                                        metrics=data))
            
            # Assess with RNA-seq full coverage
            data = {'Method': method, 'Dilution': int(1 / s),
                    'Validation': 'RNA-seq'}
            metrics.append(calc_metrics(dpsi, p, full_data['delta.mle'], full_changed,
                                        metrics=data))

    # Merge results and save    
    metrics = pd.DataFrame(metrics)
    fpath = join(RESULTS_DIR, 'GSE112037.results.txt')
    metrics.to_csv(fpath, sep='\t')
    log.write('Evaluation results written to {}'.format(fpath))
    log.finish()
    