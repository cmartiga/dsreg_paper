from csv import DictWriter
from os.path import join, exists

from sklearn.metrics.classification import f1_score, precision_score, \
    recall_score
from sklearn.metrics.cluster.supervised import v_measure_score, \
    fowlkes_mallows_score
from sklearn.metrics.ranking import roc_auc_score, roc_curve

import numpy as np
import pandas as pd
from pydSreg.settings import SIMULATIONS_DIR, RESULTS_DIR, CLUST_CONFIG_FPATH, \
    CLUST_SIMULATIONS_DIR
from pydSreg.utils import LogTrack, load_pickle


def write_traces(traces, fpath):

    df = {}
    for param, values in traces.items():
        print(param, values.shape)
        if len(values.shape) > 1:
            for i in range(values.shape[1]):
                if len(values.shape) > 2:
                    for j in range(values.shape[2]):
                        df['{}.{}.{}'.format(param, i, j)] = values[:, i, j]

                else:
                    df['{}.{}'.format(param, i)] = values[:, i]
        else:
            df[param] = values
    pd.DataFrame(df).to_csv(fpath)


if __name__ == '__main__':
    log = LogTrack()
    log.write('Start processing clustering results...')
    config = pd.read_csv(CLUST_CONFIG_FPATH).set_index('dataset_id')
    model = 'cluster_reg'

    for dataset in config.index:
        log.write('Loading dataset {}...'.format(dataset))
        fpath = join(CLUST_SIMULATIONS_DIR, '{}.{}.p'.format(dataset, model))
        if exists(fpath):
            traces = load_pickle(fpath)

            fpath = join(CLUST_SIMULATIONS_DIR,
                         '{}.{}.csv'.format(dataset, model))
            log.write('Writing results to {}...'.format(fpath))
            write_traces(traces, fpath)
