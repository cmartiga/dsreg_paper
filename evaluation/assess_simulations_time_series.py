from csv import DictWriter
from os.path import join, exists

from progressbar.bar import ProgressBar
from scipy.stats.stats import pearsonr
from sklearn.metrics.classification import (f1_score, precision_score,
                                            recall_score)
from sklearn.metrics.ranking import roc_auc_score

import numpy as np
import pandas as pd
from pydSreg.settings import RESULTS_DIR, TS_SIMULATIONS_DIR, TS_CONFIG_FPATH
from pydSreg.utils import LogTrack


def load_activities(dataset_id):
    fpath = join(TS_SIMULATIONS_DIR, '{}.theta_real.csv'.format(dataset_id))
    data = pd.read_csv(fpath, index_col=0)
    true_labels = np.any(data != 0, axis=1).astype(int)
    return(data.transpose(), pd.Series(true_labels, index=data.index))


def load_enrichment_data(fpath):
    enrichments = pd.read_csv(fpath, index_col=0)
    data = {}
    pvalues = {}
    for dataset, result, in enrichments.groupby(['dataset_id', 'method']):
        result['sig'] = np.logical_and(result['fdr'] < 0.05,
                                       result['estimate'] > 0)
        df = result.groupby(['rbp'])['sig'].any().astype(int)
        data[dataset] = df.to_dict()
        df = result.groupby(['rbp'])['fdr'].min()
        pvalues[dataset] = df.to_dict()
    return(data, pvalues)


def load_dsreg_results(dataset_id, model='time_series_gp', n_rbps=50,
                       n_times=100):
    fpath = join(TS_SIMULATIONS_DIR, '{}.{}.csv'.format(dataset_id, model))
    if not exists(fpath):
        return(None)
    data = pd.read_csv(fpath, index_col=0)
    data = np.stack([[data['theta_pred.{}.{}'.format(rbp, t)]
                      for t in range(1, n_times + 1)]
                     for rbp in range(1, n_rbps + 1)], axis=2)
    return(data)


def calc_metrics(labels, pvalues, true_labels, dataset_id, method):
    if np.all(labels == 0):
        return({'specificity': 1, 'f1': 0, 'roc_auc': 0.5,
                'precision': np.nan, 'recall': 0,
                'method': method, 'dataset_id': dataset_id})
    scores = 1 - pvalues
    scores[np.isnan(scores)] = 0
    specificity = np.logical_and(labels == true_labels,
                                 labels == 0).sum() / (1 - true_labels).sum()
    record = {'specificity': specificity,
              'f1': f1_score(true_labels, labels),
              'roc_auc': roc_auc_score(true_labels, scores),
              'precision': precision_score(true_labels, labels),
              'recall': recall_score(true_labels, labels),
              'method': method, 'dataset_id': dataset_id}
    return(record)


def calc_ts_metrics(theta, activities):
    metrics = {}
    if theta is None:
        for prefix in ['positive', 'negative']:
            metrics[prefix + '_mse'] = ''
            metrics[prefix + '_calibration'] = ''
            metrics[prefix + '_ts_width'] = ''
            metrics[prefix + '_rho'] = ''
    else:
        means = np.array(theta.mean(axis=1))
        qs = np.percentile(theta, q=[2.5, 97.5], axis=1)
        calibration = np.array(np.logical_and(qs[0] < activities,
                                              qs[1] > activities))
        active_regulators = np.any(activities != 0, axis=0)
        for prefix, mask in [('positive', active_regulators),
                             ('negative', np.logical_not(active_regulators))]:
            mse = ((means[:, mask] - activities[:, mask]) ** 2).mean().mean()
            metrics[prefix + '_mse'] = mse
            rho = pearsonr(means[:, mask].flatten(),
                           activities[:, mask].flatten())[0]
            metrics[prefix + '_rho'] = rho
            metrics[prefix + '_calibration'] = calibration[:, mask].mean()
            ts_width = np.mean(qs[1, :, mask] - qs[0, :, mask])
            metrics[prefix + '_ts_width'] = ts_width
    return(metrics)


def get_active_regulators_dsreg(theta):
    means = theta.mean(axis=1)
    mins, maxs = np.argmax(means, axis=0), np.argmin(means, axis=0)
    delta = np.array([theta[max_i, :, i] - theta[min_i, :, i]
                      for i, (max_i, min_i) in enumerate(zip(maxs, mins))])
    percentiles = [percentile / 2., 100 - percentile / 2.]
    qs = np.percentile(delta, q=percentiles, axis=1)
    labels = (1 - np.logical_and(qs[0] < 0, qs[1] > 0).astype(int)).flatten()
    p = np.min(np.vstack([(delta > 0).mean(1), (delta < 0).mean(1)]),
               axis=0).flatten()
    return(labels, p)


def get_active_regulators_clustering(dataset_enrichment,
                                     dataset_pvalues, rbps):
    labels = np.array([dataset_enrichment.get(rbp, 0) for rbp in rbps])
    p = np.array([dataset_pvalues.get(rbp, 1) for rbp in rbps])
    return(labels, p)


if __name__ == '__main__':
    clustering_methods = ['kmeans', 'spectral_clustering',
                          'hc_cor', 'hc', 'MCL']

    log = LogTrack()
    percentile = 1
    log.write('Start processing clustering results...')
    config = pd.read_csv(TS_CONFIG_FPATH).set_index('dataset_id')
    config = config.to_dict(orient='index')

    fpath = join(RESULTS_DIR, 'time_series_enrichment.csv')
    enrichment, pvalues = load_enrichment_data(fpath)

    fpath = join(RESULTS_DIR, 'time_series_results.csv')
    rocs = []
    with open(fpath, 'w') as fhand:
        fieldnames = ['dataset_id', 'specificity', 'f1', 'precision',
                      'recall', 'roc_auc', 'method', 'expected_counts',
                      'positive_calibration', 'positive_mse',
                      'positive_ts_width', 'positive_rho',
                      'negative_calibration', 'negative_mse',
                      'negative_ts_width', 'negative_rho']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()

        progressbar = ProgressBar(max_value=len(config),
                                  prefix='Datasets processed')
        config_items = progressbar(config.items())
        log.write('Start processing datasets')

        for dataset_id, dataset_config in config_items:
            # Load true clusters
            activities, true_labels = load_activities(dataset_id)
            activities = np.array(activities)
            n_rbps = dataset_config['n_regulators']
            exp_counts = dataset_config['expected_counts']

            # Load dsreg results
            for model in ['time_series_gp',
                          'time_series_reg',
                          'time_series_reg2']:
                dsreg_res = load_dsreg_results(dataset_id, model=model,
                                               n_rbps=n_rbps, n_times=100)
                if dsreg_res is None:
                    continue
                dsreg_labels, p = get_active_regulators_dsreg(dsreg_res)
                ts_record = calc_metrics(dsreg_labels, p, true_labels,
                                         dataset_id, method=model)
                ts_record.update(calc_ts_metrics(dsreg_res, activities))
                ts_record['expected_counts'] = exp_counts
                ts_record['method'] = model
                writer.writerow(ts_record)

            # Load clustering and enrichment results
            for method in clustering_methods:
                dataset_enrichment = enrichment.get((dataset_id, method), {})
                dataset_pvalues = pvalues.get((dataset_id, method), {})
                res = get_active_regulators_clustering(dataset_enrichment,
                                                       dataset_pvalues,
                                                       true_labels.index)
                labels, p = res
                ts_record = calc_metrics(labels, p, true_labels, dataset_id,
                                         method=method)
                ts_record.update(calc_ts_metrics(None, activities))
                ts_record['expected_counts'] = exp_counts
                ts_record['method'] = method
                writer.writerow(ts_record)

    log.write('Finished succesfully')
