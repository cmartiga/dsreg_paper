from itertools import groupby
from utils.settings import BRIE_IDS_FPATH, BRIE_GTF_FPATH


def parse_MISO_exon(exon):
    chrom, start, end, strand = exon.split(':')
    start = int(start) - 1
    end = int(end)
    return(chrom, start, end, strand)


def parse_MISO_id(miso_id):
    exons = [parse_MISO_exon(exon) for exon in miso_id.split('@')]
    up_exon, alt_exon, down_exon = exons

    if up_exon[1] > down_exon[1]:
        up_exon, down_exon = down_exon, up_exon
    
    chrom, start, end, strand = alt_exon
    return({'chrom': chrom, 'start': start, 'end': end, 'strand': strand,
            'up_exon_end': up_exon[2], 'down_exon_start': down_exon[1]})


def parse_BRIE_gtf_line(gtf_line):
    items = gtf_line.strip().split('\t')
    attrs = {item.strip().split(' ')[0]: item.strip().split(' ')[1].strip('"')
             for item in items[8].split(';')}
    return({'chrom': items[0], 'start': int(items[3]) - 1,
            'end': int(items[4]), 'event_type': items[1], 'feature': items[2],
            'attributes': attrs, 'strand': items[6]})


def parse_BRIE_gtf(fpath):
    
    with open(fpath) as fhand:
        for gtf_line in fhand:
            if gtf_line.startswith('#'):
                continue
            parsed_gtf = parse_BRIE_gtf_line(gtf_line)
            
            if parsed_gtf['feature'] != 'exon':
                continue
            if parsed_gtf['attributes']['transcript_id'].split('.')[-1] == 'in':
                yield(parsed_gtf)


def parse_BRIE_event(exons):
    exons = sorted(exons, key=lambda x: x['start'])
    if len(exons) != 3:
        event_id = exons[0]['attributes']['gene_id']
        print('Incorrect number of exons for {}'.format(event_id))
        return(None)
    up_exon, alt_exon, down_exon = exons
    if up_exon['start'] > down_exon['start']:
        up_exon, down_exon = down_exon, up_exon
    
    return({'chrom': alt_exon['chrom'], 'start': alt_exon['start'],
            'end': alt_exon['end'], 'strand': alt_exon['strand'],
            'up_exon_end': up_exon['end'],
            'down_exon_start': down_exon['start']})
        

def get_BRIE_exons(gtf_fpath):
    
    parsed_gtf = parse_BRIE_gtf(gtf_fpath)
    for brie_id, exons in groupby(parsed_gtf,
                                  key=lambda x: x['attributes']['gene_id']):
        event = parse_BRIE_event(exons)
        if event is not None:
            yield(brie_id, event)
        

def format_exon_id(parsed_exon):
    return('{}:{}:{}:{}:{}:{}'.format(parsed_exon['chrom'],
                                      parsed_exon['strand'], 
                                      parsed_exon['up_exon_end'],
                                      parsed_exon['start'],
                                      parsed_exon['end'],
                                      parsed_exon['down_exon_start']))


if __name__ == '__main__':
    with open(BRIE_IDS_FPATH, 'w') as fhand:
        fhand.write('brie_id,event_id\n')
        for brie_id, event in get_BRIE_exons(BRIE_GTF_FPATH):
            fhand.write('{},{}\n'.format(brie_id, format_exon_id(event)))
