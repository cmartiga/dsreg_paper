from csv import DictWriter
from os.path import join, exists
from sklearn.metrics.classification import (f1_score, precision_score,
                                            recall_score)
from sklearn.metrics.ranking import roc_auc_score, roc_curve
import numpy as np
import pandas as pd
from utils.settings import (CONFIG_FPATH, SIMULATIONS_DIR, RESULTS_DIR,
                            FDR_THRESHOLDS, DSREG_METHODS, ENRICH_METHODS, ORA,
                            GSEA, DSREG)
from utils.utils import LogTrack


def load_dataset_targets(dataset_id):
    fpath = join(SIMULATIONS_DIR, '{}.reg_effect.csv'.format(dataset_id))
    data = np.array(pd.read_csv(fpath, index_col=0).iloc[:, 0])
    return(data)


def parse_model_results(dataset_id, model='model'):
    fpath = join(SIMULATIONS_DIR, '{}.{}.csv'.format(dataset_id, model))
    if not exists(fpath):
        return(None)
    posterior = pd.read_csv(fpath, usecols=lambda x: x not in ['sigma', 'lp__'] and x.split('.')[0] not in ['alpha', 'beta'],
                            index_col=0)
    if posterior.shape[0] == 0:
        return(None)
    p = np.vstack([(posterior > 0).mean(0), (posterior < 0).mean(0)])
    p = np.max(p, axis=0)
    p = 1 - p
    data = pd.DataFrame({'p': p, 'rbp': posterior.columns, 'method': model,
                         'theta': posterior.mean(),
                         'ci_low': np.percentile(posterior, q=2.5, axis=0),
                         'ci_high': np.percentile(posterior, q=97.5, axis=0)})
    return(data)


def calc_metrics(results, reg_effects, method, fdr_threshold=0.05):
    if method in ENRICH_METHODS:
        label = 'fdr'
    elif method in DSREG_METHODS:
        label = 'p'
    else:
        raise ValueError('Invalid method: {}'.format(method))
    
    # Select ORA results made with the selected FDR threshold in dAS analysis
    if 'fdr_threshold' in results.columns:
        results = results.loc[results['fdr_threshold'] == fdr_threshold, :]
    
    # Select the most significant across Included or Skipped
    if method in ENRICH_METHODS:
        results = results.groupby(['rbp'])[label].min().reset_index()
        results['sig'] = results[label] < fdr_threshold
    
    # Order regulatory effects accordingly
    sel_idxs = [int(x) for x in results['rbp']]
    reg_effects = reg_effects[sel_idxs]
    
    # Set changes according to threshold
    targets = (reg_effects != 0).astype(int)
    ypred = results[label] < fdr_threshold
    scores = 1 - results[label]

    # Calculate metrics
    calc_calibration = 'ci_low' in results.columns
    calc_calibration = calc_calibration and 'ci_high' in results.columns
    if calc_calibration:
        calibration = np.mean(np.logical_and(results['ci_low'] < reg_effects,
                                             results['ci_high'] > reg_effects))
        
    else:
        calibration = None
        
    specificity = np.logical_and(ypred == targets,
                                 ypred == 0).sum() / (1 - targets).sum()
    record = {'specificity': specificity,
              'f1': f1_score(targets, ypred),
              'roc_auc': roc_auc_score(targets, scores),
              'precision': precision_score(targets, ypred),
              'recall': recall_score(targets, ypred),
              'method': method,
              'fdr_threshold': fdr_threshold,
              'calibration': calibration}

    fpr, tpr, _ = roc_curve(targets, scores)
    roc = pd.DataFrame({'tpr': tpr, 'fpr': fpr, 'method': method,
                        'fdr_threshold': fdr_threshold})
    return(record, roc)


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Start processing regulation results...')
    
    # Load simulation parameters
    config = pd.read_csv(CONFIG_FPATH).set_index('dataset_id')
    config = config.to_dict(orient='index')

    # Load ORA results
    fpath = join(RESULTS_DIR, 'simulations_ORA.results.csv')
    ora = pd.read_csv(fpath).fillna(1)
    ora['ci'] = None

    # Load GSEA results
    fpath = join(RESULTS_DIR, 'simulations_GSEA.results.csv')
    gsea = pd.read_csv(fpath)
    gsea['ci'] = None

    # Initialize writer for evaluation of regulation inference
    fpath = join(RESULTS_DIR, 'simulations_regulation.results.csv')
    rocs = []
    with open(fpath, 'w') as fhand:
        # FIX fieldnames
        fieldnames = ['dataset_id', 'recall', 'precision', 'specificity',
                      'f1', 'roc_auc', 'method', 'loglambda',
                      'fdr_threshold', 'calibration']
        writer = DictWriter(fhand, fieldnames=fieldnames)
        writer.writeheader()

        # Iterate over datasets
        for dataset_id, results in ora.groupby(['dataset_id']):
            log.write('Load dataset {}'.format(dataset_id))
            reg_effects = load_dataset_targets(dataset_id)
            n_regulators = config[dataset_id]['n_regulators']

            # Analyze performance of ORA
            for threshold in FDR_THRESHOLDS:
                metrics, roc = calc_metrics(results, reg_effects,
                                            method=ORA, fdr_threshold=threshold)
                metrics['dataset_id'] = dataset_id
                metrics['loglambda'] = config[dataset_id]['loglambda']
                writer.writerow(metrics)

                roc['loglambda'] = config[dataset_id]['loglambda']
                roc['dataset_id'] = dataset_id
                rocs.append(roc)

            # Parse GSEA results
            dataset_gsea = gsea.loc[gsea['dataset_id'] == dataset_id, :]
            metrics, roc = calc_metrics(dataset_gsea, reg_effects,
                                        method=GSEA)
            metrics['dataset_id'] = dataset_id
            metrics['loglambda'] = config[dataset_id]['loglambda']
            writer.writerow(metrics)

            roc['loglambda'] = config[dataset_id]['loglambda']
            roc['dataset_id'] = dataset_id
            rocs.append(roc)

            # Parse dSreg results
            dsreg_out = parse_model_results(dataset_id, model=DSREG)
            if dsreg_out is None:
                continue
            metrics, roc = calc_metrics(dsreg_out, reg_effects, method=DSREG)
            metrics['dataset_id'] = dataset_id
            metrics['loglambda'] = config[dataset_id]['loglambda']
            writer.writerow(metrics)

            roc['loglambda'] = config[dataset_id]['loglambda']
            roc['dataset_id'] = dataset_id
            rocs.append(roc)

    roc = pd.concat(rocs)
    fpath = join(RESULTS_DIR, 'simulations_regulation.roc_curves.csv')
    roc.to_csv(fpath)
    log.finish()
