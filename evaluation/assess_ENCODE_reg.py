from os.path import join, exists
from os import listdir

import numpy as np
import pandas as pd
from utils.settings import RESULTS_DIR, ENCODE_DIR
from utils.utils import LogTrack
from statsmodels.stats.multitest import multipletests
from random import shuffle


def calc_ORA_metrics(df, threshold, sel_rbp, method, mode, cell_line):
    df.sort_values('pvalue', inplace=True)
    df['rank'] = np.arange(1, df.shape[0] + 1)
    df['region'] = [x.split('.', 1)[-1] for x in df['regulator']]
    df['rbp'] = [x.split('.')[0] for x in df['regulator']]
    df['significant'] = df['fdr'] < threshold
    
    metrics = {'rbp': sel_rbp, 'total': df['significant'].sum(),
               'enrichment': np.any(np.logical_and(df['significant'],
                                                   df['rbp'] == sel_rbp)),
               'rank': np.min(df.loc[df['rbp'] == sel_rbp, 'rank']),
               'method': method, 'mode': mode, 'cell_line': cell_line}
    return(metrics)
    

def extract_logfc(df, logfc, qvalue, sel_rbp, method, mode, cell_line):
    if logfc is not None:
        enriched = np.intersect1d(df.loc[df['significant'], 'rbp'].unique(),
                                  logfc.index)
        if enriched.shape[0] == 0:
            return(None)
        rbp_logfc = logfc.loc[enriched]
        rbp_qvalue = qvalue.loc[enriched]
    else:
        return(None)
    
    rbp_logfc = pd.DataFrame({'logfc': rbp_logfc, 'qvalue':rbp_qvalue},
                             index=enriched)
    rbp_logfc['rbp'] = sel_rbp
    rbp_logfc['mode'] = mode
    rbp_logfc['method'] = method
    rbp_logfc['cell_line'] = cell_line
    return(rbp_logfc)


def calc_dsreg_df(df):
    df.drop(['sigma', 'lp__'], inplace=True, axis=1)
    
    p = np.vstack([(df > 0).mean(0), (df < 0).mean(0)])
    p = np.min(p, axis=0)
    p = 2 * p

    data = pd.DataFrame({'pvalue': p, 'regulator': df.columns,
                         'fdr': p})
    return(data)


def _load_logfc(results_dir):
    deg_dir = join(results_dir, 'expression')
    files_fpath = join(deg_dir, 'metadata.tsv')
    sel_cols = ['sample_1', 'log2(fold_change)', 'gene', 'q_value']
    df = []
    with open(files_fpath) as fhand:
        for line in fhand:
            items = line.strip().split('\t')
            file_id = items[0]
            output_type = items[2]
            if output_type != 'differential expression quantifications':
                continue
            fpath = join(deg_dir, '{}.tsv'.format(file_id))
            if not exists(fpath):
                print('Missing file {}.tsv'.format(file_id))
                continue
            d = pd.read_csv(fpath, sep='\t')
            if 'test_id' not in d.columns:
                continue
            df.append(d[sel_cols])
    df = pd.concat(df)
    
    logfc = pd.pivot_table(index='gene', columns='sample_1',
                           values='log2(fold_change)', data=df, fill_value=np.nan)
    
    qvalue = pd.pivot_table(index='gene', columns='sample_1',
                            values='q_value', data=df, fill_value=np.nan)
    
    return(logfc, qvalue)

def load_logfc(results_dir, force=False):
    fpath_l = join(results_dir, 'logfc_table.csv')
    fpath_q = join(results_dir, 'qvalue_table.csv')
    if force or not exists(fpath_l) or not exists(fpath_q):
        logfc, qvalue = _load_logfc(results_dir)
        logfc.to_csv(fpath_l)
        qvalue.to_csv(fpath_q)
    else:
        logfc = pd.read_csv(fpath_l, index_col=0)
        qvalue = pd.read_csv(fpath_q, index_col=0)
    return(logfc, qvalue)


def get_random_controls(df, logfc, qvalue, method, cell_line, threshold, sel_rbp, n=20):
    p = list(df['pvalue'])
    for _ in range(n):
        shuffle(p)
        df['pvalue'] = p 
        if method != 'dSreg':
            df['fdr'] = multipletests(df['pvalue'], method='fdr_bh')[1]
        else:
            df['fdr'] = df['pvalue']
            
        metrics = calc_ORA_metrics(df, threshold, sel_rbp,
                                   method, 'Random', cell_line)
        rbp_logfc = extract_logfc(df, logfc, qvalue, sel_rbp,
                                  method, 'Random', cell_line)
        yield(metrics, rbp_logfc)


def load_results(results_dir, logfc, qvalue, threshold=0.05):
    results = []
    results_logfc = []
    
    ora_dir = join(results_dir, 'ORA')
    gsea_dir = join(results_dir, 'GSEA')
    dsreg_dir = join(results_dir, 'dSreg')
    
    for fname in listdir(ora_dir):
        experiment = fname.split('.')[0]
        sel_rbp, _, cell_line  = experiment.split('-')
        exp_logfc = logfc[experiment] if experiment in logfc.columns else None
        exp_qvalue = qvalue[experiment] if experiment in qvalue.columns else None

        # Load dSreg results
        method = 'dSreg'
        fpath = join(dsreg_dir, fname)
        if not exists(fpath):
            continue
        df = pd.read_csv(fpath, index_col=0)
        dsreg_df = calc_dsreg_df(df)
        metrics = calc_ORA_metrics(dsreg_df, threshold, sel_rbp,
                                   method, 'Method', cell_line)

        if np.isnan(metrics['rank']):
            continue
        results.append(metrics)
        sel_cols = set(df.columns)
        
        
        rbp_logfc = extract_logfc(dsreg_df, exp_logfc, exp_qvalue, sel_rbp,
                                  method, 'Method', cell_line)
        if rbp_logfc is not None:
            results_logfc.append(rbp_logfc)
        
        # Random selection of the same number of dSreg regulators
        for metrics, rbp_logfc in get_random_controls(dsreg_df, exp_logfc,
                                                      exp_qvalue,
                                                      method, cell_line,
                                                      threshold, sel_rbp, n=20):
            results.append(metrics)
            if rbp_logfc is not None:
                results_logfc.append(rbp_logfc)
            
        # Load enrichment methods results        
        for method, fpath in [('ORA', join(ora_dir, fname)),
                              ('GSEA', join(gsea_dir, '{}.gsea.csv'.format(fname.split('.')[0])))]:
            if not exists(fpath):
                print('Missing file: {}'.format(fpath))
                continue
            df = pd.read_csv(fpath, index_col=0)
            sel_rows = [x in sel_cols for x in df['regulator']]
            df = df.loc[sel_rows, :]
            df['fdr'] = multipletests(df['pvalue'], method='fdr_bh')[1]
            metrics = calc_ORA_metrics(df, threshold, sel_rbp,
                                       method, 'Method', cell_line)
            results.append(metrics)
            
            rbp_logfc = extract_logfc(df, exp_logfc, exp_qvalue, sel_rbp,
                                      method, 'Method', cell_line)
            if rbp_logfc is not None:
                results_logfc.append(rbp_logfc)
            
            # Random selection of the same number of regulators
            for metrics, rbp_logfc in get_random_controls(df, exp_logfc,
                                                          exp_qvalue, method,
                                                          cell_line, threshold,
                                                          sel_rbp, n=20):
                results.append(metrics)
                if rbp_logfc is not None:
                    results_logfc.append(rbp_logfc)
        
    return(pd.DataFrame(results), pd.concat(results_logfc))


if __name__ == '__main__':
    log = LogTrack()
    log.write('Loading logFC...')
    logfc, qvalue = load_logfc(ENCODE_DIR, force=False)
    
    log.write('Loading ENCODE enrichment results...')
    res = load_results(ENCODE_DIR, logfc, qvalue, threshold=0.05)
    results, results_logfc = res
    
    fpath = join(RESULTS_DIR, 'encode_results.csv')
    results.to_csv(fpath)
    log.write('Enrichment assessment written to {}'.format(fpath))
    
    fpath = join(RESULTS_DIR, 'encode_results_logfc.csv')
    results_logfc.to_csv(fpath)
    log.write('Expression assessment written to {}'.format(fpath))
    log.finish()
    
    