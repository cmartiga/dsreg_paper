# dSreg-paper

This repository contains all the code required to reproduced the analysis performed for the [dSreg paper](https://www.biorxiv.org/content/10.1101/595751v1).
dSreg code and guide can be found [here](https://bitbucket.org/cmartiga/dsreg)

There are some sections (2 and 3) that are still uncomplete. Although main ideas a a part of the code is already available and can help reproduction of the results, there are some processing steps that are still missing and the whole pipeline can not be fully reproduced following the code in this repository.
Since most of the analysis performed was computationally intensive, it was performed on a computing cluster, which is highly advised should you require to reproduce every processing step. However, we do provide the fully processed data in the *results* directory to allow the complete reproduction of the paper Figures.


## 0. Setting up the requirements

Create a virtual environment and install the following dependencies

```
virtualenv -p python3 dsreg_paper
source dsreg_paper/bin/activate
pip install numpy matplotlib scipy seaborn statsmodels
```

Download this repository with the scripts and add the repository to the PYTHONPATH
```
git clone git clone git@bitbucket.org:cmartiga/dsreg_paper.git
export PYTHONPATH=$PYTHONPATH:dsreg_paper
```

## 1. Benchmarking on simulated data

In this section we analyze the performance of the statistical model used for analyzing differential splicing assuming already that we have correctly identified the alternative splicing events present in our samples and allocated the number of sequencing reads supporting the inclusion or skipping of any particular type of alternative splicing event.
In a real scenario, this task still needs to be performed by other software such as the following:

-	[MISO](https://miso.readthedocs.io/en/fastmiso/)
-	[rMATS](http://rnaseq-mats.sourceforge.net/)
-	[vast-tools](https://github.com/vastgroup/vast-tools)
-	[whippet](https://github.com/timbitz/Whippet.jl)

Whereas these tools focus on the correct identification of reads genereted by each splicing event to estimate inclusion rates in a single sample, the statistical model behind the differential inclusion analysis is rather similar, and can be represented by a Generalized Linear Model (GLM) with binomial likelihood.

### Simulate data
To first simulate the data under the same model just run the script *simulate_data.py*
```
python simulate_data.py
```
This will create a *config.csv* in data/simulations, with the conditions in which each dataset was simulated. For each dataset there are files with the true values and the observations to perform the different types of analysis.

### Differential splicing analysis
We then analyze differential splicing with a regular GLM using *Rscripts/dAS_analysis.R* and [dSreg](https://bitbucket.org/cmartiga/dsreg), using both the full and null models to test whether the inclusion of the binding sites information improves inference of splicing changes.

```
DATA_DIR=data/simulations
for i in $(seq 1 340)
do
	Rscript Rscripts/dAS_analysis $i $DATA_DIR
	
	for model in dS-PW dSreg-PW
		dSreg -I $DATA_DIR/$i.inclusion.csv -T $DATA_DIR/$i.total.csv -S $DATA_DIR/$i.binding_sites.csv -d $DATA_DIR/$i.design.csv -o $DATA_DIR/$i -a
	done
done
```

In the same *data/simulations* folder this creates a number of output files for the different analysis using the dataset prefix for each of them. To compare the performance of the different methods in detecting change in inclusion rates for the range of simulated conditions:

```
python evaluation/assess_simulations_dAS.py
```
This will create a table in the *results* directory with different evaluation metrics that will be represented in Figure 2

### Figure 2
```
python figures_scripts/fig2.py
```
![Figure 2](https://bitbucket.org/cmartiga/dsreg_paper/raw/master/figures/figure2.png)


### Regulatory analysis
Whereas [dSreg](https://bitbucket.org/cmartiga/dsreg) directly analyzes the regulatory patterns, for Over-Representation Analysis (ORA) and Gene Set Enrichment Analysis (GSEA) of the regulatory features, we need to perform an additional step taking into account the results of the differential splicing analysis with the GLM.

```
python classic_analysis/simulations_ORA.py
python classic_analysis/simulations_GSEA.py
```

And evaluate their performance for finding the regulatory elements driving the splicing changes.
```
python evaluation/assess_simulations_reg.py
```

This will generate a table in the *results* directory that will be used to produce Figure 3.

### Figure 3
```
python figures_scripts/fig3.py
```
![Figure 3](https://bitbucket.org/cmartiga/dsreg_paper/raw/master/figures/figure3.png)


## 2. Benchmarking differential splicing analysis with real data

In this section we aim to compare the performance of [dSreg](https://bitbucket.org/cmartiga/dsreg) with other existing tools in terms of characterization of splicing changes using now real data. We will use two methods to represent the different versions of the more classic analysis:

-	[rMATS](http://rnaseq-mats.sourceforge.net/)-like GLM. 
-	[MISO](https://miso.readthedocs.io/en/fastmiso/) uses a bayesian approach to model the reads generation process from a predefined set of splicing events and estimates the inclusion rate for each competing isoform. Then, differential splicing is performed by means of bayesian model comparison using Bayes Factors. As it does not allow to use replicates information, reads coming from the same biological condition are merged together

However, we specially wanted to compare [dSreg](https://bitbucket.org/cmartiga/dsreg) with other tools using some type of prior information about the underlying regulation to improve the inference of splicing changes. 

-	[BRIE](https://github.com/huangyh09/brie) was meant to analyze splicing rates in single cell RNA-seq experiments, in which very few reads per splicing event and cell can be found, but use information across cells to better estimate inclusion rates. Then, differential splicing is also performed calculating Bayes Factors for models with and without differences in splicing rates between conditions.
-	[DARTS](https://github.com/Xinglab/DARTS) uses Deep Learning to build a model with regulatory features both in CIS and TRANS (expression of regulatory elements) from external data. The prediction from this model is then used as informative prior for the differential splicing analysis


### [GSE112037](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE112037) dataset and subsampling

This dataset was generated for [DARTS paper](https://www.nature.com/articles/s41592-019-0351-9) and includes regular high-depth RNA-seq with RASL-seq data for a reduced number of splicing events to be used as gold standard for the estimation of splicing changes. We also assess the impact of sequencing depth by serial downsampling RNA-seq reads.

For GLM, [DARTS](https://github.com/Xinglab/DARTS) and [dSreg](https://bitbucket.org/cmartiga/dsreg) analyses we directly used event definitions and counts as provided in GEO processed files. Downsampling of counts was carried out with the following script:
```
python subssample_GSE112037.py
```

For [MISO](https://miso.readthedocs.io/en/fastmiso/) and [BRIE](https://github.com/huangyh09/brie), fastq files were mapped using [STAR](https://github.com/alexdobin/STAR) to Ensembl version 75 genome and annotations and the BAM was subsequently downsampled using [samtools](http://www.htslib.org/)

```
cd data/GSE112037/bams
subsamples="0.5 0.25 0.125 0.0625 0.03125 0.015625 0.0078125 0.00390625 0.001953125"
for s in $subsamples
do
	for sample in  $(cat samples)
	do
		samtools view -s $sample.bam -bh > $sample.$s.bam	
		samtools index $sample.$s.bam
	done
done
```

### Running methods

The following is only a guide on how the different programs were run. Specific environments and indexes are required for each method and must be build previously.

```
GEO_DIR=data/GSE112037
GLM_DIR=$GEO_DIR/GLM
BAMS_DIR=$GEO_DIR/bams
BRIE_DIR=$GEO_DIR/BRIE
MISO_DIR=$GEO_DIR/MISO
KALLISTO_DIR=$GEO_DIR/kallisto
DARTS_DIR=$GEO_DIR/DARTS
DSREG_DIR=$GEO_DIR/dsreg

for s in $subsamples
do
	# GLM
	Rscript $GLM/differential_AS.R $s

	# MISO
	miso --run miso_index $BAMS_DIR/group1.$s.bam --output-dir $MISO_DIR/group1.$s --read-len 101 --paired-end 170 50 -p 4
	miso --run miso_index $BAMS_DIR/group2.$s.bam --output-dir $MISO_DIR/group2.$s --read-len 101 --paired-end 170 50 -p 4
	compare_miso --compare-samples $MISO_DIR/group1.$s $MISO_DIR/group2.$s $MISO_DIR/group1_vs_group2.$s

	# BRIE
	bams1=$BAMS_DIR/SRR6862379.$s.bam,$BAMS_DIR/SRR6862380.$s.bam,$BAMS_DIR/SRR6862381.$s.bam
	bams2=$BAMS_DIR/SRR6862382.$s.bam,$BAMS_DIR/SRR6862383.$s.bam,$BAMS_DIR/SRR6862384.$s.bam
	brie -a brie_index -s $bams1 -f brie_features -o $BRIE_DIR/group1.$s -p 4
	brie -a brie_index -s $bams2 -f brie_features -o $BRIE_DIR/group2.$s -p 4
	brie-diff -i $BRIE_DIR/group1.$s/samples.csv.gz,$BRIE_DIR/group2.$s/samples.csv.gz -o $BRIE_DIR/differential_AS.$s -p 4 --minBF 0
	
	# DARTS-flat
	Darts_BHT bayes_infer --darts-count $GEO_DIR/sample.$s.darts.txt  --od $DARTS_DIR/sample.$s -t SE --nthread 4
	
	# DARTS-info
	kallisto1=$KALLISTO_DIR/PCE_rep1,$KALLISTO_DIR/PCE_rep2,$KALLISTO_DIR/PCE_rep3
	kallisto2=$KALIISTO_DIR/GS689_rep1,$KALIISTO_DIR/GS689_rep2,$KALIISTO_DIR/GS689_rep3
	Darts_DNN build_feature -i $DARTS_DIR/sample.$s/SE.flat.txt -c SE.norm.txt.gz -e $kallisto1 $kallisto2 -o $DARTS_DIR/features.$s.txt -t $EVENT
	Darts_DNN predict -i $DARTS_DIR/features.$s.txt  -o $DARTS_DIR/pred.$s.txt -t SE
	Darts_BHT bayes_infer --darts-count $GEO_DIR/sample.$s.darts.txt  --od $DARTS_DIR/sample.$s -t SE --nthread 4 --prior $DARTS_DIR/pred.$s.txt
	
	# Run dS
	dSreg -I $GEO_DIR/sample.$s.inclusion.csv -T $GEO_DIR/sample.$s.total.csv -S $DSREG_DIR/clip_counts.csv  -d $DSREG_DIR/design.csv -m 1 -s 1 -ms 0 -mi 0 -b -L $DSREG_DIR/lengths.csv -a -o $DSREG_DIR/ds_pw.$s.2000 -r $DSREG_DIR/regions -M dS-PW -k $DSREG_DIR/RASL_events.txt -K 2000  --seed 0
	
	# Run dSreg
	dSreg -I $GEO_DIR/sample.$s.inclusion.csv -T $GEO_DIR/sample.$s.total.csv -S $DSREG_DIR/clip_counts.csv  -d $DSREG_DIR/design.csv -m 1 -s 1 -ms 0 -mi 0 -b -L $DSREG_DIR/lengths.csv -a -o $DSREG_DIR/dsreg_pw.$s.2000 -r $DSREG_DIR/regions -M dSreg-PW -k $DSREG_DIR/RASL_events.txt -K 2000  --seed 0
	
done
```

### Evaluation

For evaluation we first need to match ids of exon skipping events from BRIE and DARTS
```
python evaluation/match_event_ids.py
```

To compute perfomance metrics run the following script
```
python evaluation/assess_GSE112037_dAS.py
```

This will create a new file *results/GSE112037.results.txt* that is readily available and is required for Figure S1 and later for Figure 4

### Figure S1
```
python figures_scripts/figS1.py
```
![Figure S1](https://bitbucket.org/cmartiga/dsreg_paper/raw/master/figures/figureS1.png)


## 3. Benchmarking regulatory analysis on [ENCODE](https://www.encodeproject.org/) dataset

To analyze the performance of [dSreg](https://bitbucket.org/cmartiga/dsreg) and other approaches in finding the regulatory drivers of splicing changes, we used data from systematic knock-down of over 200 RNA binding proteins deposited from the [ENCODE](https://www.encodeproject.org/) project. We directly downloaded data processed with [rMATS](http://rnaseq-mats.sourceforge.net/) and extracted the inclusion and total counts for analysis using classic and dSreg pipelines. As regulatory features, we used the matched CLiP-seq data for the different RBPs and identified the binding sites in the upstream and downstream intronic flanks of the exon skipping events detected by [rMATS](http://rnaseq-mats.sourceforge.net/) in each knock-down experiment.

### Running regulatory analysis
```
ENCODE_DIR=data/ENCODE
COUNTS_DIR=$ENCODE_DIR/counts_matrices
S_DIR=$ENCODE_DIR/sites_matrices
OUTDIR=$ENCODE_DIR/dSreg

for d in $(cat datasets)
do
	dSreg -I $COUNTS_DIR/$d.inclusion.csv -T $COUNTS_DIR/$d.total.csv -S $S_DIR/$d.clip_counts.csv -d $COUNTS_DIR/$d.design.csv -m 20 -r $REGIONS_FILE -me 10 -s 4 -o 4 -b
done
``` 

### Evaluation
Regulatory elements evaluation
```
python evaluation/assess_ENCODE_reg.py
```

This will generate files in *results* dir that are already provided and allow the generation of Figure 4

### Figure 4
```
python figures_scripts/fig4.py
```
![Figure 4](https://bitbucket.org/cmartiga/dsreg_paper/raw/master/figures/figure4.png)

## 4. New insights into AS regulation during CM maturation

Once validated the good performance of [dSreg](https://bitbucket.org/cmartiga/dsreg) in simulated and real data, we applied it to study AS changes during cardiomyocytes maturation (dataset). This dataset was aimed to study gene expression changes, adn therefore the sequencing depth is not as high as studies focused on AS itself, a scenario in which [dSreg](https://bitbucket.org/cmartiga/dsreg) can provide better insights than previous approaches.

### Regulatory analysis

```
Rscript Rscripts/dAS_analysis_data.R
python classic_analysis/data_ORA.py
python classic_analysis/data_GSEA.py

PREFIX=data/cm_maturation/exon_cassette
dSreg -I $PREFIX.inclusion.csv -T $PREFIX.total.csv -S $PREFIX.binding_sites.csv -d $PREFIX.design.csv -m 0 -me 0 -s 0  -o $PREFIX -t 4 -n 4000 
```

### Figure 5
```
python figures_scripts/fig4.py
```
![Figure 5](https://bitbucket.org/cmartiga/dsreg_paper/raw/master/figures/figure5.png)
