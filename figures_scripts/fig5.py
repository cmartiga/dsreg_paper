from os.path import join
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from utils.plot_utils import add_panel_label, arrange_plot, FIGURES_FORMAT, DPI
from utils.settings import PLOTS_DIR, CM_DIR, RESULTS_DIR, DSREG
from utils.utils import LogTrack


def rename_region(region):
    return(region.split('.')[0].replace('_', ' ').capitalize())


def add_rbp_and_region_cols(df, fieldname='rbp'):
    df['rbp_name'] = [x.split('.', 1)[0] for x in df[fieldname]]
    df['region'] = [rename_region(x.split('.', 1)[1]) for x in df[fieldname]]



def load_fit_data():
    fpath = join(CM_DIR, 'exon_cassette.binding_sites.csv')
    binding_sites = pd.read_csv(fpath).set_index('event_id')

    fpath = join(CM_DIR, 'exon_cassette.{}.csv'.format(DSREG))
    fit_data = pd.read_csv(fpath, usecols=lambda x: x.startswith('reg_effects'))
    rbp_idx = [int(x.split('.')[-1]) - 1 for x in fit_data.columns]
    fit_data.columns = binding_sites.columns[rbp_idx]
    sel_cols = np.logical_or(fit_data.quantile(q=0.025, axis=0) > 0,
                             fit_data.quantile(q=0.975, axis=0) < 0)
    sel_rbps = fit_data.columns[sel_cols]
    fit_data = pd.melt(fit_data)
    add_rbp_and_region_cols(fit_data, fieldname='variable')
    return(fit_data, sel_rbps)


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Running script to generate Figure 5')
    dataset = 'cm_maturation.exon_cassette'
    
    # Load ORA
    log.write('Loading and formatting ORA results...')
    fpath = join(RESULTS_DIR, '{}.ORA.csv'.format(dataset))
    ora = pd.read_csv(fpath, index_col=0)
    add_rbp_and_region_cols(ora)
    ora = ora.loc[ora['fdr_threshold'] == 0.05, :]
    included = ora.loc[ora['group'] == 'Included', :]
    skipped = ora.loc[ora['group'] == 'Skipped', :]
    included['logpval'] = -np.log10(included['fdr'])
    skipped['logpval'] = np.log10(skipped['fdr'])

    # Load GSEA
    log.write('Loading and formatting GSEA results...')
    fpath = join(RESULTS_DIR, '{}.gsea.csv'.format(dataset))
    gsea = pd.read_csv(fpath, index_col=0)
    add_rbp_and_region_cols(gsea)
    gsea['logpval'] = np.log10(gsea['pvalue']) * np.sign(gsea['estimate'])

    # Load dSreg
    log.write('Loading and formatting {} results...'.format(DSREG))
    fit_data, sel_rbps = load_fit_data()
    means = fit_data.groupby(['variable'])['value'].mean().sort_values()
    sorted_rbps = list(means.index)

    # Figure
    log.write('Setting figure')
    sns.set(style="white")
    fig = plt.figure(figsize=(10, 7))
    gs = GridSpec(85, 76)

    # Plot results from classical analyses
    ax = fig.add_subplot(gs[:, :22])
    log.write('Plotting ORA results')
    sns.barplot(y='rbp', x='logpval', data=included, color='blue',
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    sns.barplot(y='rbp', x='logpval', data=skipped, color='red',
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    ax.set_xticks([-20, -10, 0, 10])
    ax.set_xticklabels(['20', '10', '0', '10'])
    ylims = ax.get_ylim()
    ax.plot((0, 0), ylims, linewidth=1.5, color='black')
    arrange_plot(ax, ylabel=r'RBP-region',
                 xlabel=r'$-log_{10}(FDR)$',
                 showlegend=False, vline=[-2, 2], despine=True, despine_left=True,
                 title='ORA')
    add_panel_label(ax, 'A', xfactor=0.2, yfactor=0.05)

    # Plot results from GSEA
    log.write('Plotting GSEA results')
    ax = fig.add_subplot(gs[:, 27:49])
    sns.barplot(y='rbp', x='logpval', data=gsea,
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    ax.set_xticks([-2, 0, 2])
    ax.set_xticklabels(['2', '0', '2'])
    ylims = ax.get_ylim()
    ax.plot((0, 0), ylims, linewidth=1.5, color='black')
    arrange_plot(ax, ylabel='', title='GSEA',
                 xlabel=r'$-log_{10}(p-value)$',
                 showlegend=False, vline=[-2, 2], despine=True, despine_left=True)
    add_panel_label(ax, 'B', xfactor=0.2, yfactor=0.05)

    # Plot model results
    log.write('Plotting {} results'.format(DSREG))
    ax = fig.add_subplot(gs[:, 54:76])
    sns.boxplot(y='variable', x='value', data=fit_data, showfliers=False,
                ax=ax, orient='horizontal', linewidth=0.5, order=sorted_rbps)
    ax.set_yticklabels([])
    arrange_plot(ax, ylabel='', title='dSreg',
                 xlabel=r'Regulatory effect $\theta_{j}$',
                 showlegend=False, vline=0, despine=True, despine_left=True)
    add_panel_label(ax, 'C', xfactor=0.2, yfactor=0.05)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure5.{}'.format(FIGURES_FORMAT))
    fig.savefig(fpath, format=FIGURES_FORMAT, dpi=DPI)
    log.write('Figure 5 saved at {}'.format(fpath))
    
    # Store data
    gsea.set_index('rbp', inplace=True)
    included.set_index('rbp', inplace=True)
    skipped.set_index('rbp', inplace=True)

    df = pd.DataFrame({'rbp': sel_rbps,
                       'Included': included.loc[sel_rbps, 'fdr'],
                       'Skipped': skipped.loc[sel_rbps, 'fdr'],
                       'GSEA': gsea.loc[sel_rbps, 'pvalue'],
                       'dSreg': means.loc[sel_rbps]})
    add_rbp_and_region_cols(df)
    fpath = join(RESULTS_DIR, '{}.active_regulators.csv'.format(dataset))
    df.to_csv(fpath)
    log.write('Relevant regulators table written to {}'.format(fpath))
    log.finish()
