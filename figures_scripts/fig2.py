from os.path import join

from matplotlib.gridspec import GridSpec

import matplotlib.pyplot as plt
import pandas as pd
from utils.plot_utils import (CLASS_COLORS, add_panel_label, arrange_plot,
                              get_panel_subplots, FIGURES_FORMAT, DPI)
from utils.settings import (RESULTS_DIR, PLOTS_DIR, DSREG, NULL, GLM, DSREG_METHODS,
                            FDR_THRESHOLDS)
from utils.utils import LogTrack
import seaborn as sns


def _get_method_label(df):
    labels = []
    for method, threshold in zip(df['method'], df['threshold']):
        if method in DSREG_METHODS:
            label = method
        elif method == GLM:
            label = 'GLM-FDR<{}'.format(threshold)
        else:
            label = 'None'
        labels.append(label)
    return(labels)
        


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Running script to generate Figure 2')
    
    # Load metrics
    fpath = join(RESULTS_DIR, 'simulations_dAS.results.csv')
    results = pd.read_csv(fpath)
    msg = 'Loaded evaluation results from differential splicing from {}'
    log.write(msg.format(fpath))
    results = results.loc[results['dataset_id'] < 200, :]
    log.write('Filtered 200 datasets corresponding to variable coverage')
    results['label'] = _get_method_label(results)
    datasets = set(results['dataset_id'])

    # Load ROC curves
    fpath = join(RESULTS_DIR, 'simulations_dAS.roc_curves.csv')
    roc = pd.read_csv(fpath, index_col=0)
    sel_rows = [dataset in datasets for dataset in roc['dataset_id']]
    roc = roc.loc[sel_rows, :]
    roc['label'] = _get_method_label(roc)
    log.write('Loaded ROC curves from {}'.format(fpath))

    # Figure
    hue_order = ['GLM-FDR<{}'.format(t) for t in sorted(FDR_THRESHOLDS)[::-1]]
    hue_order += DSREG_METHODS
    methods = [GLM, NULL, DSREG]

    sns.set(style="white")
    fig = plt.figure(figsize=(14, 8))
    gs = GridSpec(100, 100)

    # Plot correlation coefficient
    log.write('Plotting Pearson coefficient')
    axes = fig.add_subplot(gs[:40, :25])
    sns.boxplot(x='loglambda', y='rho', hue='method', data=results,
                hue_order=methods, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$',
                 showlegend=False, ylabel=r'Pearson $\rho$', ticklabels_size=8)
    add_panel_label(axes, 'A', xfactor=0.25)

    # Plot sensitivity
    log.write('Plotting sensitivity')
    axes = fig.add_subplot(gs[:40, 35:60])
    sns.boxplot(x='loglambda', y='recall', hue='label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='Sensitivity',
                 ticklabels_size=8)
    axes.legend(loc=(-0.5, 1.2), frameon=True, fancybox=True, ncol=4,
                fontsize=10)
    add_panel_label(axes, 'B', xfactor=0.3)

    # Plot F1 score
    log.write('Plotting F1 score')
    axes = fig.add_subplot(gs[:40, 70:])
    sns.boxplot(x='loglambda', y='f1', hue='label', data=results,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='F1 score',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'C', xfactor=0.3)

    # Plot ROC curves
    log.write('Plotting ROC curves')
    subplots = get_panel_subplots(fig, gs, nrow=2, ncol=5, xs=[0, 60],
                                  ys=[55, 100], yspace=4, xspace=2)
    for i, (loglambda, subdata) in enumerate(roc.groupby(['loglambda'])):
        axes = subplots[i]
        for method, df in subdata.groupby(['method']):
            for _, dataset_roc in df.groupby(['dataset_id']):
                axes.plot(dataset_roc['fpr'], dataset_roc['tpr'],
                          c=CLASS_COLORS[method], lw=0.5, alpha=0.3)

        ylabel = 'True Positive Rate'
        xlabel = ''
        if i == 7:
            xlabel = 'False Positive Rate'
        if i < 5:
            axes.set_xticklabels([])
        if i % 5 != 0:
            ylabel = ''
            axes.set_yticklabels([])
            
        axes.plot((0, 1), (0, 1), color='black', lw=0.3, linestyle='--')
        arrange_plot(axes, xlims=(0, 1), ylims=(0, 1),
                     xlabel=xlabel, ylabel=ylabel, ticklabels_size=7,
                     fontsize=9, despine=False)
        axes.set_title(r'$log(\lambda)$ = {}'.format(loglambda), fontsize=7)
    add_panel_label(subplots[0], 'D', xfactor=0.78, yfactor=0.2)

    # Plot ROC AUC
    log.write('Plotting AUROC')
    axes = fig.add_subplot(gs[55:, 70:])
    sns.boxplot(x='loglambda', y='roc_auc', hue='method', data=results,
                hue_order=methods, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel='AUROC',
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'E', xfactor=0.25, yfactor=0.1)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure2.{}'.format(FIGURES_FORMAT))
    fig.savefig(fpath, format=FIGURES_FORMAT, dpi=DPI)
