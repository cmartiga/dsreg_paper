from os.path import join
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import pandas as pd
from utils.plot_utils import (CLASS_COLORS, add_panel_label, arrange_plot,
                              TYPES_COLORS, FIGURES_FORMAT, DPI)
from utils.settings import (RESULTS_DIR, PLOTS_DIR, CONFIG_FPATH,
                            AUROC, F1, DSREG, GSEA, ORA)
import seaborn as sns
from utils.utils import LogTrack


if __name__ == '__main__':
    # Initialize log
    log = LogTrack()
    log.write('Plotting figure 3...')
    
    # Load simulation parameters
    config = pd.read_csv(CONFIG_FPATH)
    regs = config.set_index('dataset_id')['n_regulators'].to_dict()
    log.write('Loaded simulation parameters')
    
    # Load splicing changes evaluation metrics
    fpath = join(RESULTS_DIR, 'simulations_dAS.results.csv')
    events = pd.read_csv(fpath)
    events = events.loc[events['method'] == DSREG, :]
    events['n_regulators'] = [regs[dataset]
                              for dataset in events['dataset_id']]
    events['Type'] = 'Event'
    events1 = events.loc[events['dataset_id'] <= 200, :]
    events2 = events.loc[events['dataset_id'] > 200, :]
    log.write('Loaded events evaluations from {}'.format(fpath))

    # Load regulatory changes evaluation metrics
    fpath = join(RESULTS_DIR, 'simulations_regulation.results.csv')
    results = pd.read_csv(fpath)
    results = results.loc[results['fdr_threshold'] == 0.05, :]
    results['n_regulators'] = [regs[dataset]
                               for dataset in results['dataset_id']]
    results['Type'] = 'Regulator'
    results1 = results.loc[results['dataset_id'] <= 200, :]
    results2 = results.loc[results['dataset_id'] > 200, :]
    merged1 = pd.concat([results1, events1])
    merged2 = pd.concat([results2, events2])
    log.write('Loaded regulators evaluations from {}'.format(fpath))

    # Figure
    hue_order = [ORA, GSEA, DSREG]
    sns.set(style="white")
    fig = plt.figure(figsize=(8, 6))
    gs = GridSpec(90, 60)

    # Plot AUROC vs loglambda
    log.write('Plotting regulators identification AUROC')
    axes = fig.add_subplot(gs[:38, :25])
    sns.boxplot(x='loglambda', y='f1', hue='method', data=results1,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$', ylabel=AUROC,
                 showlegend=False, ticklabels_size=8)
    axes.legend(loc=(0.6, 1.2), frameon=True, fancybox=True, ncol=3,
                fontsize=10)
    add_panel_label(axes, 'A', xfactor=0.3)

    # Plot AUROC vs N regulators
    axes = fig.add_subplot(gs[:38, 35:60])
    sns.boxplot(x='n_regulators', y='f1', hue='method', data=results2,
                hue_order=hue_order, palette=CLASS_COLORS, showfliers=False,
                ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel='Number of regulators', ylabel=F1,
                 showlegend=False, ticklabels_size=8)
    add_panel_label(axes, 'B', xfactor=0.25)

    # Plot calibration for events
    ylims = (0.6, 1)
    axes = fig.add_subplot(gs[52:90, :25])
    sns.boxplot(x='loglambda', y='calibration', hue='Type', data=merged1,
                palette=TYPES_COLORS, showfliers=False, ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel=r'$log(\lambda)$',
                 ylabel='Calibration',
                 showlegend=False, ticklabels_size=8,
                 hline=0.95, ylims=ylims)
    axes.legend(loc=4, frameon=True, fancybox=True)
    add_panel_label(axes, 'C', xfactor=0.3)

    # Plot calibration for events
    axes = fig.add_subplot(gs[52:90, 35:60])
    sns.boxplot(x='n_regulators', y='calibration', hue='Type', data=merged2,
                palette=TYPES_COLORS, showfliers=False, ax=axes)
    sns.despine(ax=axes)
    arrange_plot(axes, xlabel='Number of regulators',
                 ylabel='Calibration',
                 showlegend=False, ticklabels_size=8,
                 hline=0.95, ylims=ylims)
    add_panel_label(axes, 'D', xfactor=0.25)

    # Save figure
    fpath = join(PLOTS_DIR, 'figure3.{}'.format(FIGURES_FORMAT))
    fig.savefig(fpath, format=FIGURES_FORMAT, dpi=DPI)
    log.finish()
