from os.path import join
import pandas as pd
import seaborn as sns
from utils.settings import RESULTS_DIR, PLOTS_DIR
from utils.plot_utils import init_fig, arrange_plot, savefig, FIGURES_FORMAT


if __name__ == '__main__':
    data = pd.read_csv(join(RESULTS_DIR, 'GSE112037.results.txt'), sep='\t')
    hue_order = ['GLM', 'MISO', 'BRIE', 'DARTS-flat', 'DARTS-info',
                 'dS-PW', 'dSreg-PW']
    
    # Figure
    fig, subplots = init_fig(4, 2, colsize=4)

    # Plot sensitivity analysis
    for i, validation in enumerate(['RASL-seq', 'RNA-seq']):
        subdata = data.loc[data['Validation'] == validation, :]
        for j, metric in enumerate(['rho', 'MSE', 'F1 score', 'AUROC']):
            
            axes = subplots[j][i]
            sns.barplot(x='Dilution', y=metric, hue='Method',
                        data=subdata, n_boot=0, ax=axes, hue_order=hue_order)
            if metric == 'rho':
                metric = r'Pearson $\rho$'
            arrange_plot(axes, xlabel='Depth dilution factor',
                         ylabel='{} {}'.format(validation, metric),
                         showlegend= i==0 and j == 1,
                         legend_loc=2, legend_frame=True, fontsize=9,
                         despine=True, yticklines=True)
    
    # Save figure
    fpath = join(PLOTS_DIR, 'figureS1.{}'.format(FIGURES_FORMAT))
    fig.tight_layout()
    savefig(fig, fpath)
