from os.path import join
from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from utils.settings import RESULTS_DIR, PLOTS_DIR 
from utils.plot_utils import (arrange_plot, add_panel_label, savefig,
                              FIGURES_FORMAT)
from scipy.stats.morestats import wilcoxon


def load_encode_results():
    fpath = join(RESULTS_DIR, 'encode_results.csv')
    data = pd.read_csv(fpath, index_col=0)

    fpath = join(RESULTS_DIR, 'encode_results_logfc.csv')
    logfc = pd.read_csv(fpath) 
    logfc['abs'] = np.abs(logfc['logfc'])
    logfc['deg'] = np.logical_and(logfc['qvalue'] < 0.01,
                              logfc['abs'] > 0.5)
    deg = logfc.groupby(['method', 'mode'])['deg'].mean().reset_index()
    deg['deg'] = deg['deg'] * 100
    deg = pd.pivot_table(data=deg, values='deg', index='method',
                         columns='mode')
    deg['ratio'] = deg['Method'] / deg['Random']
    deg['method'] = deg.index
    
    sens = data.groupby(['method', 'mode'])['enrichment'].mean().reset_index()
    sens = pd.pivot_table(data=sens, index='method', columns='mode',
                          values='enrichment')
    sens['ratio'] = sens['Method'] / sens['Random']
    sens['method'] = sens.index
    
    rank = pd.pivot_table(data=data.loc[data['mode'] == 'Method', :],
                          index='rbp', columns='method', values='rank')
    rank['deltaORA'] = rank['dSreg'] - rank['ORA']
    rank['deltaGSEA'] = rank['dSreg'] - rank['GSEA']
    return(sens, deg, rank)


if __name__ == '__main__':
    # Load deltaPSI assessment
    dpsi = pd.read_csv(join(RESULTS_DIR, 'GSE112037.results.txt'), sep='\t')
    dpsi = dpsi.loc[dpsi['Validation'] == 'RASL-seq', :]
    dpsi['Method'].replace('dS-PW', 'Null model', inplace=True)
    
    # Load RBPs activity assesment
    rbps = load_encode_results()
    
    # Plotting parameters
    palette = {'Method': 'purple', 'Random': 'orange'}
    xorder = ['ORA', 'GSEA', 'dSreg']
    
    # Figure
    sns.set(style="white")
    fig = plt.figure(figsize=(10, 7))
    gs = GridSpec(100, 100)

    # Plot Pearson correlations
    axes = fig.add_subplot(gs[5:50, :48])
    hue_order = ['GLM', 'MISO', 'BRIE', 'DARTS-flat', 'DARTS-info',
                 'Null model', 'dSreg-PW']
    sns.barplot(x='Dilution', y='rho', hue='Method',
                data=dpsi, n_boot=0, ax=axes, hue_order=hue_order)
    ylabel = r'GSE112037 Pearson $\rho$ ($\Delta\Psi_{RASL},\bar{\Delta\Psi}$)'
    arrange_plot(axes, xlabel='Depth dilution factor',
                 ylabel=ylabel, despine=True, yticklines=True, fontsize=10)
    axes.legend(loc=(0.1, 1.11), frameon=True, fancybox=True,
                ncol=len(hue_order), fontsize=10)
    add_panel_label(axes, 'A', xfactor=0.2, yfactor=0)
    
    # Plot Pearson AUROC of for splicing changes
    axes = fig.add_subplot(gs[5:50, -48:])
    sns.barplot(x='Dilution', y='AUROC', hue='Method',
                data=dpsi, n_boot=0, ax=axes, hue_order=hue_order)
    arrange_plot(axes, xlabel='Depth dilution factor', fontsize=10,
                 ylabel='GSE112037 ROC-AUC ($|\Delta\Psi| > 0.05$)',
                 despine=True, yticklines=True, ylims=(0, 0.9))
    add_panel_label(axes, 'B', xfactor=0.2, yfactor=0)
    
    # Proportion of times RBP that was KD was found among regulators
    axes = fig.add_subplot(gs[58:, :23])
    sns.barplot(x='method', y='ratio', data=rbps[0], ax=axes,
                linewidth=1, n_boot=0, order=xorder, color='purple')
    arrange_plot(axes, ylabel=r'% KD-RBPs detected / expected', xlabel='Method',
                 yticklines=True, despine=True, rotation=45,
                 rotate_xlabels=True, fontsize=10)
    add_panel_label(axes, 'C', xfactor=0.54, yfactor=0.12)
    
    # Proportion of detected RBPs that were detected also to be DE
    axes = fig.add_subplot(gs[58:, 25:48])
    sns.barplot(x='method', y='ratio', data=rbps[1], ax=axes,
                linewidth=1, n_boot=0, order=xorder, color='purple')
    arrange_plot(axes, ylabel=r'% RBPs detected and DE / expected',
                 xlabel='Method', yticklines=True, despine=True, rotation=45,
                 rotate_xlabels=True, fontsize=10)
    add_panel_label(axes, 'D', xfactor=0.54, yfactor=0.12)
    
    # Plot rank differences with ORA
    axes = fig.add_subplot(gs[58:, -48:-25])
    sns.distplot(rbps[2]['deltaORA'], ax=axes, bins=10, norm_hist=False,
                 kde=False, hist_kws={'edgecolor': 'white', 'linewidth': 1.5})
    arrange_plot(axes, xlabel=r'Rank$_{dSreg}$ - Rank$_{ORA}$', fontsize=10,
                 ylabel='Number of experiments', despine=True, vline=0)
    add_panel_label(axes, 'E', xfactor=0.54, yfactor=0.12)
    pvalue = wilcoxon(rbps[2]['deltaORA'])[1]
    axes.text(-215, 26, 'p={:.2f}'.format(pvalue))
    
    # Plot rank differences with GSEA
    axes = fig.add_subplot(gs[58:, -23:])
    sns.distplot(rbps[2]['deltaGSEA'].dropna(),
                 ax=axes, bins=10, norm_hist=False,
                 kde=False, hist_kws={'edgecolor': 'white', 'linewidth': 1.5})
    arrange_plot(axes, xlabel=r'Rank$_{dSreg}$ - Rank$_{GSEA}$', fontsize=10,
                 ylabel='Number of experiments', despine=True, vline=0)
    add_panel_label(axes, 'F', xfactor=0.54, yfactor=0.12)
    pvalue = wilcoxon(rbps[2]['deltaGSEA'])[1]
    axes.text(-232, 21, 'p={:.2f}'.format(pvalue))
    
    # Save figure
    fpath = join(PLOTS_DIR, 'figure4.{}'.format(FIGURES_FORMAT))
    fig.tight_layout()
    savefig(fig, fpath)
