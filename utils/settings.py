from os.path import join, abspath, dirname

# Project directories
CM_MATURATION = 'cm_maturation'
PROJECT_DIR = abspath(join(dirname(__file__), '..'))

DATA_DIR = join(PROJECT_DIR, 'data')
PLOTS_DIR = join(PROJECT_DIR, 'figures')
RESULTS_DIR = join(PROJECT_DIR, 'results')

SIMULATIONS_DIR = join(DATA_DIR, 'simulations')
ENCODE_DIR = join(DATA_DIR, 'ENCODE')
CM_DIR = join(DATA_DIR, CM_MATURATION)

GEO_DIR = join(DATA_DIR, 'GSE112037')
DARTS_DIR = join(GEO_DIR, 'DARTS')
GLM_DIR = join(GEO_DIR, 'GLM')
DSREG_DIR = join(GEO_DIR, 'dsreg')
BRIE_DIR = join(GEO_DIR, 'BRIE')
MISO_DIR = join(GEO_DIR, 'miso')

# File paths
BRIE_IDS_FPATH = join(BRIE_DIR, 'brie_ids.csv')
BRIE_GTF_FPATH = join(BRIE_DIR, 'SE.filtered.gtf')
CONFIG_FPATH = join(SIMULATIONS_DIR, 'config.csv')

# Labels
ORA = 'ORA'
GSEA = 'GSEA'
DSREG = 'dSreg-PW'
NULL = 'dS-PW'
GLM = 'GLM'
DSREG_METHODS = [NULL, DSREG]
ENRICH_METHODS = [ORA, GSEA]

AUROC = 'AUROC'
F1 = 'F1 score'
SENSITIVITY = 'Sensitivity'
SPECIFICITY = 'Specificity'
PRECISION = 'Precision'

FDR_THRESHOLDS = [0.05, 0.2]
