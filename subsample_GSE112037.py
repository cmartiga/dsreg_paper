from os.path import join

import numpy as np
import pandas as pd
from utils.settings import GEO_DIR


def to_DARTS_format(inclusion, skipping, lengths, design=None):
    if design is None:
        group1 = list(range(int(inclusion.shape[1] / 2)))
        group2 = list(range(int(inclusion.shape[1] / 2), inclusion.shape[1]))
    
    i1 = [','.join(row.astype(str)) for row in inclusion.iloc[:, group1].as_matrix()]
    i2 = [','.join(row.astype(str)) for row in inclusion.iloc[:, group2].as_matrix()]  
    s1 = [','.join(row.astype(str)) for row in skipping.iloc[:, group1].as_matrix()]
    s2 = [','.join(row.astype(str)) for row in skipping.iloc[:, group2].as_matrix()]
    
    data = pd.DataFrame({'I1': i1, 'I2': i2, 'S1': s1, 'S2': s2,
                         'inc_len': lengths['inc_len'], 'ID': inclusion.index,
                         'skp_len': lengths['skp_len']})
    colnames = ['ID', 'I1', 'S1', 'I2', 'S2', 'inc_len', 'skp_len']
    return(data[colnames])

if __name__ == '__main__':
    np.random.seed(0)
    subsampling = [2 ** (-x) for x in range(1, 10)]
    
    total = pd.read_csv(join(GEO_DIR, 'dsreg', 'total.csv'), index_col=0).astype(int)
    inclusion = pd.read_csv(join(GEO_DIR, 'dsreg', 'inclusion.csv'), index_col=0).astype(int)
    skipping = total.as_matrix() - inclusion.as_matrix()
    lengths = pd.read_csv(join(GEO_DIR, 'dsreg', 'lengths.csv'), index_col=0).astype(int)
    
    for s in subsampling:
        sinclusion = pd.DataFrame(np.random.poisson(inclusion * s),
                                  index=total.index,
                                  columns=total.columns)
        sinclusion.to_csv(join(GEO_DIR, 'sample.{}.inclusion.csv'.format(s)))
        
        sskipping = pd.DataFrame(np.random.poisson(skipping * s), index=total.index,
                                 columns=total.columns)
        stotal = sinclusion + sskipping
        stotal.to_csv(join(GEO_DIR, 'sample.{}.total.csv'.format(s)))
        
        data = to_DARTS_format(sinclusion, sskipping, lengths)
        fpath = join(GEO_DIR, 'sample.{}.darts.txt'.format(s))
        data.to_csv(fpath, index=False, sep='\t')
        